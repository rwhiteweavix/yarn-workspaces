# About Workspaces

Workspaces are pretty easy to use. To add a new workspace, create a new folder under workspaces/ and add a package.json file.
Then use yarn to add dependencies.

To use another workspace within your current workspace, run `yarn add [other-workspace-name]` inside your workspace folder.

You can treat each workspace folder as if it was its own git repo, or you can keep your terminal in the top-level folder
and run commands on individual workspaces using `yarn workspace [workspace-name] [add|start|run|etc...]`
